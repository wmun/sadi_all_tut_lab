package Tutelab03.test;

import Tutelab03.birds.Eagle;
import Tutelab03.birds.Parrot;
import Tutelab03.interfaces.Flyer;
import Tutelab03.interfaces.FlyingHero;
import Tutelab03.interfaces.SuperHero;
import Tutelab03.superheroes.*;

// sample solution to TuteLab03 superhero problem by Caspar
public class TestHeirarchies {
    // used by both methods
    private FlyingHero superman = new Superman();
    private FlyingHero birdman = new Birdman();

    public static void main(String[] args) {
        TestHeirarchies test = new TestHeirarchies();
        test.superHeroes();
        test.combinedFly();
    }

    // just the superheros
    public void superHeroes() {
        // only used locally
        SuperHero aquaman = new Aquaman();
        SuperHero batman = new Batman();
        SuperHero wonderwoman = new WonderWoman();

        // populate the arrays so we can loop them (no casting/type
        // conversion required!)
        SuperHero[] superHeroes = new SuperHero[]
                {aquaman, batman, wonderwoman, superman, birdman};
        FlyingHero[] flyingHeroes = new FlyingHero[]
                {superman, birdman};

        // All heroes
        for (SuperHero superhero : superHeroes)
            superhero.saveTheWorld();

        // FlyingHeroes only
        for (FlyingHero flyingHero : flyingHeroes)
            flyingHero.fly();

        System.out.println();
    }

    // flying birds and heroes together!
    public void combinedFly() {
        // only used locally
        Flyer eagle = new Eagle();
        Flyer parrot = new Parrot();

        // all of the flyers combined
        Flyer[] flyers = new Flyer[]
                {eagle, parrot, superman, birdman};

        for (Flyer flyer : flyers)
            flyer.fly();

        System.out.println();
    }
}
