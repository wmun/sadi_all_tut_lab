package Tutelab03.birds;

public class Eagle extends AbstractFlyingBird {
    @Override
    public void fly() {
        System.out.println("The eagle is soaring");
    }
}
