package Tutelab03.interfaces;

// this interface contains the methods of the interfaces it extends
public interface FlyingHero extends SuperHero, Flyer {

}
