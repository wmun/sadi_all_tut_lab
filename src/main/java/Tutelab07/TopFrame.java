package Tutelab07;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class TopFrame extends JToolBar {
    public JToolBar TopFrame() {
        JToolBar jToolBar = new JToolBar();
        for (int i = 0; i < 5; i++) {
            Random random = new Random();
            int min = 1, max = 5;
            int randomNumber = ThreadLocalRandom.current().nextInt(min, max + 1);
            JButton button = new JButton(new ImageIcon("src/main/resources/images/circle_" + randomNumber + ".png"));

            Border border = BorderFactory.createLineBorder(Color.GREEN);
            button.setBorder(border);

            button.setPreferredSize(new Dimension(100, 100));
            jToolBar.add(button);
        }
        return jToolBar;
    }


}
