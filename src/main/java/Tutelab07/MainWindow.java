package Tutelab07;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends SimpleJFrame {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    MainWindow frame = new MainWindow();
                    frame.setLayout(new BorderLayout());

                    JToolBar jToolBar = new TopFrame();

                    /* add toolbar to frame */
                    frame.add(jToolBar, BorderLayout.NORTH);

                    /*add center panel to main frame*/
                    frame.add(new CenterFrame(), BorderLayout.CENTER);


                    JFrame jFrame = new JFrame();
                    FlowLayout flowLayout = new FlowLayout();
                    jFrame.setLayout(flowLayout);


                    frame.pack(); //resize the window to fit all components
                    frame.showFrame();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
    }


//        jLabel.
//        flowLayout.addLayoutComponent(jLabel);
//        showFrame();


}
