package Tutelab07;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class CenterFrame extends JPanel {

    public CenterFrame() {
        super();
        setLayout(new GridLayout(0, 6));

        JLabel[] images = new JLabel[21];

        for (int i = 0; i < images.length; i++) {
            images[i] = new JLabel(this.randomGenerateImg(0, 5));
            add(images[i]);
        }

//        return this;
    }

    public ImageIcon randomGenerateImg(int min, int max) {
        int randomNumber = ThreadLocalRandom.current().nextInt(min, max + 1);
        return new ImageIcon("src/main/resources/images/circle_" + randomNumber + ".png");
    }

}
