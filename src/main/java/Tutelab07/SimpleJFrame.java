package Tutelab07;

import javax.swing.*;

public abstract class SimpleJFrame extends JFrame {

    public void showFrame() {
        // set a fixed size (would normally read from config file)
        setBounds(100, 100, 800, 600);
        // so we can close the frame on exit (when 'x' clicked in UI)
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // make it visible
        setVisible(true);
    }

}
